# Practica 1 - Diseño de Sistemas Inteligentes #

Repositorio de codigo.

### ARCHIVOS ###

Preprocesamineto.py -> Precosesamiento de los datos y exportacion a .txt para Matlab
Test.m -> Lectura de los datos .txt, Clustering y exportación de los resultados (Cluster y pertenencia).
BIC.m-> Funcion matlab para calcular K.
ProcesarResultados.py -> Carga de los resultados con los datos originales y estudio de los resultados

El resto de los archivos son archivos intermedios, auxiliares, imagenes o archivos generados por Python o Matlab.