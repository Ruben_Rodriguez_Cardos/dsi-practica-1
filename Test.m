close all, clear, clc   % cerrrar  ventanas gr�ficas 
                        % borrar memoria y  consola

%% Leer el fichero con los datos
filename = 'data.txt';
data = textread(filename, '', 'delimiter', ',', 'emptyvalue', NaN);
  
%%Calculo del K
Kmax=20;    % n�mero m�ximo de cluster a analizar
for K=2:Kmax
    [cidx] = kmeans(data, K,'Replicates',10);
    [Bic_K,xi]=BIC(K,cidx,data);
    BICK(K)=Bic_K;
end

figure(1)
plot(2:K',BICK(2:K)','s-','MarkerSize',6,...
     'MarkerEdgeColor','r', 'MarkerFaceColor','r')
xlabel('K','fontsize',18)      % etiquetado del eje-x
ylabel('BIC(K)','fontsize',18) % etiquetado del eje-y

%%El indice con el valor mas peque�o del vector BICK sera el numero de
%%clusters
K=-1;
aux = 1;
for i=1:length(BICK)
  if BICK(i) < aux
    K = i;
    aux=BICK(i);
  end
end

%%Fuzzy C Means
                           % n�mero de cluster
m=2;                       % par�metro de fcm, 2 es el defecto
MaxIteraciones=100;        % n�mero de iteraciones
Tolerancia= 1e-5;          % tolerancia en el criterio de para
Visualizacion=0;           % 0/1
opciones=[m,MaxIteraciones,Visualizacion];
[center,U,obj_fcn] = fcm(data, K,opciones);
%%%%%%%
% p a r � m e t r o s   d e   s a l i d a              
% center    centroides de los grupos
% U         matriz de pertenencia individuo cluster 
% obj_fun   funci�n objetivo
%%%%%%  Asignaci�n de individuo a grupo, maximizando el nivel de
%       pertenencia al grupo
for i=1:K
maxU=max(U); % calculo del m�ximo nivel de pertenencia de los
             % individuos
individuos=find(U(i,:)==maxU);% calcula los individuos del
                              % grupo i que alcanzan el m�ximo
cidx(individuos)=i;           % asigna estos individuos al grupo i
grado_pertenencia(individuos)=maxU(individuos);
end
%% Representaci�n de individuos
figure(2)
%%Grupo 1
%%plot(data(cidx==1,1),data(cidx==1,2),'rs','MarkerSize',4,'MarkerEdgeColor','b','MarkerFaceColor','b');
%%Grupo 2
%%hold on
%%plot(data(cidx==2,1),data(cidx==2,2),'bo',...
%% 'MarkerSize',6,'MarkerEdgeColor','b', 'MarkerFaceColor','b');
colors = ["y","m","c","r","g","b","k"]
shapes = ["+","o","*",".","rs","x","d"]
cs = 1
for i=1:K
  hold on
  plot(data(cidx==i,1),data(cidx==i,2),shapes(cs),'MarkerSize',2,...
                  'MarkerEdgeColor',colors(cs), ...
                  'MarkerFaceColor',colors(cs));
  cs = cs+1;
  if cs > length(colors)
     cs = 1; 
  end
end

xlabel('x_1','fontsize',18),ylabel('x_2','fontsize',18)
legend('Grupo 1'),axis('square'), box on
title('Algoritmo Fuzzy c-means','fontsize',18)
% Escritura del nivel de pertenencia de cada individuo
for i=1:size(data,1)
    %text(data(i,1),data(i,2),num2str(grado_pertenecia(i)),'FontSize',16);
end
print(1,'-depsc','resul_fcm')   % genera gr�fico .eps en fichero   


%%ESCRIBIR RESULTADOS EN UN FICHERO, CLUSTER Y % PERTENENCIA

fileID = fopen('Cluster.txt','w');
for i1=1:length(cidx)
  e1 = cidx(i1);
  fprintf(fileID,'%d\n',e1);
end
fclose(fileID);

fileID = fopen('Pertenencia.txt','w');
for i2=1:length(grado_pertenencia)
  e2 = num2str(grado_pertenencia(i2));
  fprintf(fileID,'%d\n',e2);
end
fclose(fileID);
