import pandas as pd
from sklearn import preprocessing
from sklearn.decomposition import PCA
import numpy

import matplotlib.pyplot as plt
import skfuzzy as fuzz

import operator

def printDictOfDict(d):
	for key, value in d.items():
		print("Cluster ",key)
		for key2, value2 in value.items():
			print("\t",key2,value2)	



def main():
	#Carga de los datos
	data=pd.read_excel("Datos.xlsx")
	

	#Se quitan las columnas innecesarias, en este caso no desechamos ninguna aracteristica.
	data.columns=data.ix[[7]].values.tolist()[0]
	data=data[8:]
	data=data.drop(columns="Código INE municipio (5 dígitos)")
	data=data[data["Índice de Gini "] != 'n.d.']
	colsToDrop=["Código INE municipio",			
			 	"Nombre Provincia",
				"Índice de Gini ", 
				"Código INE Comunidad Autónoma ",
			 	"Código INE Provincia ",
	            "Renta imponible agregada (IRPF)",
	            "Población declarante (IRPF)",
	            "Número de observaciones muestrales",
	            "Renta mediana",
	            "Top 0,5%"]
	#data=data.drop(columns=colsToDrop)
	print(data.columns)
	print()

	#Renombrar columnas (PARA QUITAR ACENTOS Y CARACTERES PROBLEMATICOS)
	data = data.rename(columns={'Población (INE)': 'Poblacion', 'Índice de Atkinson 0,5': 'Indice de Atkinson 0,5'})
	
	#Leer Resultados del clustering
	cluster = []
	pertenencia = []

	file = open("Cluster.txt", "r") 
	c=file.read().splitlines()
	for i in c:
		cluster.append(int(i))
	
	file = open("Pertenencia.txt", "r") 
	c=file.read().splitlines()
	for i in c:
		pertenencia.append(int(i))	

	pertenencia = pertenencia[0:1172]
	#print(len(pertenencia))

	#Añado el cluster y el grado de pertenencia al data set
	data["Cluster"]=cluster
	data["Pertenencia"]=pertenencia

	#Obtener el numero de clusters
	n_clusters = data['Cluster'].max()

	#Informacion de cada cluster
	for i in range(1,n_clusters+1):

		#Me quedo con los municipios del cluster
		aux = data.loc[data['Cluster'] == i]
		
		#Informacion
		print("Cluster: "+str(i))
		print("Pertencia media: ",aux["Pertenencia"].mean())
		print(aux["Nombre Comunidad Autónoma"].value_counts().keys()[0])
		print(aux["Nombre Provincia"].value_counts().keys()[0])
		print("Indice de Atkinson: ",aux["Indice de Atkinson 0,5"].mean())
		print("Renta imponible media (por habitante) del cluster: ",aux["Renta imponible media (por habitante)"].mean())
		print("Media Quintil 5: ",aux["Quintil 5"].mean())
		print("Media Poblacion: ",aux["Poblacion"].mean())
		print("Media Poblacion Declarante: ",aux["Población declarante (IRPF)"].mean())
		print()

	#Guardo los datos en XLSX (SI ES NECESARIO)
	#writer = pd.ExcelWriter('output.xlsx')
	#data.to_excel(writer,'Sheet1')
	#writer.save()











if __name__ == '__main__':
	main()