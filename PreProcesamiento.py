import pandas as pd
from sklearn import preprocessing
from sklearn.decomposition import PCA
import numpy

import matplotlib.pyplot as plt
import skfuzzy as fuzz


def main():
	#Carga de los datos
	data=pd.read_excel("Datos.xlsx")
	

	#Se limpian columnas innecesarias
	data.columns=data.ix[[7]].values.tolist()[0]
	data=data[8:]
	data=data.drop(columns="Código INE municipio (5 dígitos)")
	data=data[data["Índice de Gini "] != 'n.d.']
	colsToDrop=["Nombre municipio",
				"Código INE municipio",
			 	"Nombre Comunidad Autónoma",
			 	"Nombre Provincia",
				"Índice de Gini ", 
				"Código INE Comunidad Autónoma ",
			 	"Código INE Provincia ",
	            "Población declarante (IRPF)",
	            "Número de observaciones muestrales",
	            "Renta imponible media (por declarante)",
	            "Renta mediana",
	            "Top 1%",
	            "Top 0,1%",
	            "Top 0,5%"]
	data=data.drop(columns=colsToDrop)
	print(len(data.columns))
	print(data.columns)

	#Renombrar columnas (PARA QUITAR ACENTOS Y CARACTERES PROBLEMATICOS)
	data = data.rename(columns={'Población (INE)': 'Poblacion', 'Índice de Atkinson 0,5': 'Indice de Atkinson 0,5'})
	
	#Guardarmos los datos en CSV
	data.to_csv('Datos_filtrados.csv',index=False)

	#Preprocesamiento
	min_max_scaler = preprocessing.MinMaxScaler()
	states = min_max_scaler.fit_transform(data)

	#2 Dimensiones 77% de los datos son representados
	estimator = PCA (n_components = 2)
	X_pca = estimator.fit_transform(states)
	print(estimator.explained_variance_ratio_)
	#new_data=pd.DataFrame(numpy.matrix.transpose(estimator.components_), columns=['PC-1', 'PC-2'], index=data.columns)
	#print(new_data)

	
	#Guardo el resultado de PCA para pasarlo a MATLAB
	file = open("data.txt","w")
	for i in X_pca:
		aux=str(i[0])+", "+str(i[1])+"\n"
		file.write(aux)
	file.close()

if __name__ == '__main__':
	main()