%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ejemplo   d e t e c c i � n   d e  K
%           m � t o d o   B I C
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all, clear, clc   % cerrrar  ventanas gr�ficas 
                        % borrar memoria y  consola
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% G e n e r a c i � n   d e   d a t o s K*=4
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
sigma=0.4;  % desviaci�n t�pica de la variable normal empleada
            % en la generaci�n de datos
n=200;       % n�mero de datos por grupo
Kmax=20;    % n�mero m�ximo de cluster a analizar
X = [sigma*randn(n,2)+ones(n,2); 
     sigma*randn(n,2)-ones(n,2);
     sigma*randn(n,2)+[ones(n,1),-ones(n,1)]; 
     sigma*randn(n,2)+[-ones(n,1),ones(n,1)] ];
figure(1)   % creaci�n de una ventana gr�fica
hold on     % para dibujar varias gr�ficas en la misma ventana
% Dibuja los objetos del primer grupo (sr->cudrados rojos)
color={'b','r','y','k'}; % definici�n de un color para cada grupo
forma={'o','s','*','v'}; % definicion de una forma
for k=1:4
    datos=(1+(k-1)*n):(k*n);
    plot(X(datos,1),X(datos,2),forma{k},'MarkerSize',6,...
                 'MarkerEdgeColor',color{k}, 'MarkerFaceColor','r')
     axis('square')      % ejes cuadrados 
     box on              % dibuja bordes en la figura
     hold on             % dibuja varias gr�ficas en la misma figura
end
xlabel('x_1','fontsize',18) % etiquetado del eje-x
ylabel('x_2','fontsize',18) % etiquetado del eje-y
title('Generaci�n de Datos','fontsize',18)% t�tulo del gr�fico
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% C � l c u l o   d e l   B I C
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for K=2:Kmax
    [cidx] = kmeans(X, K,'Replicates',10);
    [Bic_K,xi]=BIC(K,cidx,X);
    BICK(K)=Bic_K;
end
figure(2)
plot(2:K',BICK(2:K)','s-','MarkerSize',6,...
     'MarkerEdgeColor','r', 'MarkerFaceColor','r')
xlabel('K','fontsize',18)      % etiquetado del eje-x
ylabel('BIC(K)','fontsize',18) % etiquetado del eje-y

